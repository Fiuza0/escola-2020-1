package br.ucsal.bes20192.testequalidade.escola.business;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoBuilder {
	 
    public static final String DEFAULT_NAME = "Rick Sanches";
    public static final Integer DEFAULT_ANO = 2003;
    public static final SituacaoAluno DEFAULT_SITUACAO = SituacaoAluno.ATIVO;
 
    private Integer matricula = 1;

	private String nome = DEFAULT_NAME;

	private SituacaoAluno situacao = DEFAULT_SITUACAO;

	private Integer anoNascimento = DEFAULT_ANO;

	public AlunoDAO dao = new AlunoDAO();
 
    public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public SituacaoAluno getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAluno situacao) {
		this.situacao = situacao;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	private AlunoBuilder() {
    }
 
    public static AlunoBuilder aAluno() {
        return new AlunoBuilder();
    }
 
    public AlunoBuilder comNome(String name) {
        this.nome = name;
        return this;
    }
 
    public AlunoBuilder comSituacao(SituacaoAluno situacao) {
        this.situacao = situacao;
        return this;
    }
 
    public AlunoBuilder semSituacao() {
        this.situacao = null;
        return this;
    }
    public AlunoBuilder comMatricula() {
        this.matricula = matricula;
        return this;
    }
    public AlunoBuilder comAnoNascimento() {
        this.anoNascimento = anoNascimento;
        return this;
    }
 
 
    public AlunoBuilder but() {
        return AlunoBuilder
                .aAluno()
                .comNome(nome)
                .comSituacao(situacao);
                
    }
 
    public Aluno build() {
    	Aluno aluno = new Aluno();
    	aluno.setAnoNascimento(anoNascimento);
    	aluno.setMatricula(matricula);
    	aluno.setNome(nome);
    	aluno.setSituacao(situacao);
    	dao.salvar(aluno);
        return aluno;
    }
}