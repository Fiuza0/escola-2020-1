package br.ucsal.bes20192.testequalidade.escola.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste 
	 * # | entrada 				 | saida esperada 
	 * 1 | aluno nascido em 2003 |	 * 16
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		AlunoBuilder aluno = AlunoBuilder.aAluno();
		aluno.build();
		DateHelper data = new DateHelper();
	    AlunoBO al = new AlunoBO(aluno.dao, data);
	    int valor = al.calcularIdade(aluno.getMatricula());
	    assertEquals(16, valor);

	}
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		AlunoBuilder alunob = AlunoBuilder.aAluno();
		Aluno aluno = alunob.build();
		DateHelper data = new DateHelper();
	    AlunoBO al = new AlunoBO(alunob.dao, data);
	    Aluno alunoAntes = aluno;
	    al.atualizar(aluno);
	    assertNotEquals(alunoAntes, aluno);
	    
	}

}
